package com.example.globalexceptionhandler.controller;

import com.example.globalexceptionhandler.entity.User;
import com.example.globalexceptionhandler.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yang_li
 */
@Slf4j
@RestController
@RequestMapping("exception")
public class ExceptionController implements HandlerExceptionResolver {

    /**
     * 测试自定义的异常
     */

    @Resource
    private UserMapper userMapper;


    /**
     * 这里测试  直接在controller中使用 @ExceptionHandler(value = NullPointerException.class) 来进行异常捕获
     * 这种方式 返回的是modelAndView   需要自定义view（jsp）  不推荐使用
     * @return
     */
    @ExceptionHandler(value = NullPointerException.class)
    @GetMapping("myException")
    public ModelAndView handleNullPointerException() {
        ModelAndView modelAndView = new ModelAndView("errorView");
        modelAndView.addObject("errorMessage", "发生空指针异常");
        return modelAndView;
    }

    @GetMapping("getById")
    public ModelAndView getById(@RequestParam Integer userId) {
        // 手动抛出异常
       String string = null;
       string.replace("string","");

        User user = userMapper.selectById(userId);

        ModelAndView modelAndView = new ModelAndView("successView");
        modelAndView.addObject("user", user);

        return modelAndView;
    }

    /**
     * 这里是全局异常的另外一种实现  实现HandlerExceptionResolver接口  重写resolveException方法  即可
     * 这种方式 返回的是modelAndView   需要自定义view(jsp)  不推荐使用
     * @param request .
     * @param response .
     * @param handler .
     * @param ex .
     * @return .
     */
    @Override
    public ModelAndView resolveException(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, Object handler, @NotNull Exception ex) {
        return null;
    }
}
