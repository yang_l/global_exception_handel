package com.example.globalexceptionhandler.controller;

import com.example.globalexceptionhandler.entity.User;
import com.example.globalexceptionhandler.globalexception.MyException;
import com.example.globalexceptionhandler.globalexception.ExceptionEnum;
import com.example.globalexceptionhandler.globalexception.ResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author yang_li
 */
@RestController
@RequestMapping("exception")
@Slf4j
public class GlobalExceptionTest {

    @PostMapping("/add")
    public boolean add(@RequestBody User user) {
        //如果姓名为空就手动抛出一个自定义的异常！
        if (user.getName() == null || "".equals(user.getName())) {
            throw new MyException("-1", "用户姓名不能为空！");
        }
        return true;
    }

    @PutMapping("/update")
    public boolean update() {
        //这里故意造成一个空指针的异常，并且不进行处理
        String str = null;
        "111".equals(str);
        return true;
    }

    @DeleteMapping("/delete")
    public boolean delete() {
        //这里故意造成一个异常，并且不进行处理
        Integer.parseInt("abc123");
        return true;
    }

    /**
     * 处理类型转换异常
     *
     * @param e .
     * @return .
     */

    @ResponseBody
    @GetMapping("/get")
    public ResultResponse exceptionHandler(NumberFormatException e) {
        log.error("发生类型转换异常！原因是:", e);
        return ResultResponse.error(ExceptionEnum.PARAMS_NOT_CONVERT);
    }


}
