package com.example.globalexceptionhandler.entity;

import lombok.Data;

/**
 * @author yang_li
 */
@Data
public class User {

    private Integer id;

    private String name;
}
