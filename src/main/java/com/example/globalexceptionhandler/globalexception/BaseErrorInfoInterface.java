package com.example.globalexceptionhandler.globalexception;


/**
 * @author yang_li
 */
public interface BaseErrorInfoInterface {

    /**
     * 错误码
     *
     * @return 1
     */
    String getCode();

    /**
     * 错误描述
     *
     * @return 1
     */
    String getMsg();
}
