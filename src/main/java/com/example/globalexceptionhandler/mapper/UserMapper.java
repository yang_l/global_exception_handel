package com.example.globalexceptionhandler.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.globalexceptionhandler.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yang_li
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
